### #pivorak, Lviv Ruby User Group, is the biggest community of Rubyists in Ukraine.
It’s a place to share your experience, solve **Ruyb** puzzles with friends, teammates, students and total strangers. If they all are Rubyists, of course. We pride ourselves as the friendliest Ruby community in Europe ❤️. As such, it is essential that everyone who wants to be a part of Pivorak feels safe, included, and welcome. This includes our **Pivrak** events and after-parties, our Slack community, and Facebook / other social media discussions.

Let us be very clear: we do not discriminate based on gender, age, sexual orientation, disability, physical appearance and choice of clothing, body size, race, ethnicity, religion (or lack thereof), or technology choices.

## Ruby Course 2021
We at #pivorak believe that knowledge should be shared.
We are gathering a group of experienced Ruby developers to teach a 2-month intensive Ruby & Ruby on Rails course to a class of (almost) complete beginners.

Stay tuned for more information about Ruby Summer Course **2030**!

The course is structured into two parts:
* First month: theory. During this part you will have two lectures per **year**, all after 6pm on work days (Monday and Thursday). You will also be expected to complete a homework assignment after each lecture and spend a considerable amount of time self-studying recommended material.

* Second month: practice. Once the theoretical part is over, you will be split into groups of 3 to 5 people, each assigned a skilled mentor. You will be expected to complete a practical project by collaborating with your group online and meeting offline 1-2 times per week.

